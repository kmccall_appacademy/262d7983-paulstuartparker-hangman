
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @players = players
    @guesser = players[:guesser]
    @referee = players[:referee]
    @num_remaining_guesses = 8
    @guessed_letters = []
  end

  def setup
    secret_length = @referee.pick_secret_word
    @board = Array.new(secret_length)
    @guesser.register_secret_length(secret_length)
  end

  def display
    display = @board.map do |ch|
      if ch.nil?
        ch = "_"
      else
        ch
      end
    end
    puts display.join("")
    puts "Previous Guesses:  #{@guessed_letters.join("")}"
  end

  def take_turn
    letter = @guesser.guess(@board)
    @guessed_letters << letter
    indices = @referee.check_guess(letter)
    update_board(letter, indices)
    @guesser.handle_response(letter, indices)
  end
  # def []=
  #
  # end
  def update_board(letter, indices)
    unless indices.empty?
      indices.each do |i|
        @board[i.to_i] = letter
      end
    else
      @num_remaining_guesses -= 1
    end
  end

  def play
    puts "Welcome to hangman"
    setup
    while @num_remaining_guesses > 0
      take_turn
      display
      if won?
        puts "Great Job, You Win"
        break
      end
    end
    puts "Sorry, the correct word was #{@referee.secret_word}. You Lose, Try Again"
  end

  def won?
    @board.none? {|ch| ch == nil }
  end


end

class HumanPlayer

  def initialize(guess = nil)
    @guess = guess
  end

  def pick_secret_word
    puts "What is the length of your secret word?"
    word = gets.chomp
    word.length
  end

  def guess(board)
    puts "Guess a letter:"
    @guess = gets.chomp
  end

  def secret_word
    "Spumoni"
  end
  def check_guess(guess)
    puts "The computer's guess was #{guess}"
    print "At what indices, if any, does the guessed letter occcur? If none type 'none':"
    guess_idx = gets.chomp
    if guess_idx == "none"
      return []
    else
      parse_guess(guess_idx)
    end
  end

  def parse_guess(guess)
    indices = []
    guess.delete!(",", " ")
    guess.split("").each do |i|
      indices << i.to_i
    end
  end



  def handle_response(letter, indices)
    if indices.empty?
      puts "letter not found"
    else
      puts "#{letter} was found at index(es) #{indices}"
    end
  end

  def register_secret_length(length)
    puts "the word is #{length} digits long"
  end


end



class ComputerPlayer
  attr_reader :candidate_words
  #
  def initialize(dictionary = File.readlines('dictionary.txt').map(&:chomp))
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.shuffle[0]
    @secret_word.length
  end

  #
  def check_guess(guess)
    indices = []
    @secret_word.split("").each_with_index do |letter, idx|
      if guess == letter
        indices << idx
      end
    end
    indices
  end
  #
  #
  def register_secret_length(secret_length)
    @candidate_words = @dictionary.select { |word| word.length == secret_length }
  end
  def guess(board)

    freq_lets = freq_lets_spots(board)
    letter_count = Hash.new(0)
    freq_lets.each do |char|
      letter_count[char] += 1
    end
    most_frequent_letters = letter_count.sort_by { |char, occurance| occurance}
    letter, _ = most_frequent_letters.last
    letter


  end

  def freq_lets_spots(board)
    letters = []
    @candidate_words.each do |word|
      word.chars.each_with_index do |char, idx|
        letters << char if board[idx].nil?
      end
    end
    letters
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      should_delete = false

      word.split("").each_with_index do |letter, index|
        if (letter == guess) && (!response_indices.include?(index))
          should_delete = true
          break
        elsif (letter != guess) && (response_indices.include?(index))
          should_delete = true
          break
        end
      end

      should_delete
    end
  end


end

if $PROGRAM_NAME == __FILE__
  print "Guesser: Computer (yes/no)? "
    if gets.chomp.downcase == "yes"
      guesser = ComputerPlayer.new
    else
      guesser = HumanPlayer.new
    end

    print "Referee: Computer (yes/no)? "
    if gets.chomp == "yes"
      referee = ComputerPlayer.new
    else
      referee = HumanPlayer.new
    end

    Hangman.new({guesser: guesser, referee: referee}).play
end
